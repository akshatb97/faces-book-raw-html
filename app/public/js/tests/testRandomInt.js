'use strict';

module.exports = testRandomInt;

const randomInt = require('./../randomInt.js');
const assert = require('assert');

function testRandomInt() {
  test_randomInt_with_lower_bound_included();
  test_randomInt_with_upper_bound_excluded();
  test_randomInt_all_the_numbers_are_covered();
}

function test_randomInt_with_lower_bound_included() {
  let count = 0;
  for (let n = 0; n < 1000; n++) {
    if ( 1 === randomInt(1,9) ) {
      count++;
    }
  }
  assert.ok(count > 0);
}

function test_randomInt_with_upper_bound_excluded() {
  let count = 0;
  for (let n = 0; n < 1000; n++) {
    if ( 9 === randomInt(1,9) ) {
      count++;
    }
  }
  assert.ok(count === 0);
}

function test_randomInt_all_the_numbers_are_covered() {
  let count = [0,0,0,0,0,0,0,0,0];
  for (let n = 0; n < 1000; n++) {
    count[randomInt(1,10)-1]++;
  }
  assert.ok(count[0] > 0);
  assert.ok(count[1] > 0);
  assert.ok(count[2] > 0);
  assert.ok(count[3] > 0);
  assert.ok(count[4] > 0);
  assert.ok(count[5] > 0);
  assert.ok(count[6] > 0);
  assert.ok(count[7] > 0);
  assert.ok(count[8] > 0);
}

testRandomInt();
