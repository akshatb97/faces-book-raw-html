'use strict';

module.exports = testing_shuffler;

const shuffler = require('./../shuffler.js');
const assert = require('assert');

//assert.equal(shuffle(), 42 );

function testing_shuffler() {
  let alphabets = ["A","B","C","D","E"];
  shuffler(alphabets);
  console.log(alphabets);
  assert.notDeepEqual(["A","B","C","D","E"],alphabets);
}
