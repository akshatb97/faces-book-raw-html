const shuffler = require('./shuffler.js');
$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded

  const faces = $('faces');
  //const add = $('<b>').html('Hello');
  //faces.append(add);
  const makeFace = function(name) {
    return $('<img>' , {
      src:`img/2018/${name}.jpg`,
      class:"face",
      title:name
    });
  };

  var names = ["Akshat", "Karthika", "Sanjana", "Mahidher", "Akhil", "Keerthana",
        "RVishnuPriya", "Anushree", "Mariah", "Aishu", "Rishi", "Jon", "Pavithrann", "Sindhu",
        "Shruti", "Santhosh", "Supriya", "Gayatri", "Vishnu", "Varsha", "John", "Ameya",
        "Sudeep", "Janani", "Thamizh", "Thiru"];

  //shuffler(names);

  names.forEach(function(name) {
    faces.append(makeFace(name));
  });
});
