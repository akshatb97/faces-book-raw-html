'use strict';

module.exports = shuffler;

const randomInt = require('./randomInt.js');

function shuffler(array) {
  let low = 0;
  let high = array.length;
  for (let n = 0; n < array.length; n++) {
    let dummy = randomInt(low,high);
    let temp = array[n];
    array[n] = array[dummy];
    array[dummy] = temp;
  }
}
